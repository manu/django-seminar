from datetime import *

from django import VERSION
from django.conf import settings
from django.http import *
from django.shortcuts import *
from django.template import RequestContext
from django.utils.translation import gettext_lazy as _
from django.views.generic import *

if VERSION >= (2, 0):
    from django.urls import reverse
else:
    from django.core.urlresolvers import reverse

from django_ical.views import ICalFeed

from seminar.models import *
from seminar.forms import *

def event_next (request, template_name='seminar/events_next.html'):
    today = date.today()
    set = Event.objects.filter(date_end__gte = today)
    next_events = set.order_by('date_begin').iterator()
    context = {}
    try:
        context['event'] = next(next_events)
    except StopIteration:
        context['event'] = None
    return render(request, template_name, context=context)

class EventMixin (object):
    model = Event
    queryset = Event.objects.all()
    form_class = EventForm

    def get_object (self, queryset=None):
        if queryset is None:
            queryset = self.get_queryset()
        slug = self.kwargs.get('event_slug')
        slug_field = self.get_slug_field()
        queryset = queryset.filter(**{slug_field: slug})
        try:
            obj = queryset.get()
        except ObjectDoesNotExist:
            raise Http404(_("No %(verbose_name)s found matching the query") %
                          {'verbose_name': queryset.model._meta.verbose_name})
        return obj

    def get_success_url (self):
        return reverse('seminar-event-list')

class EventKindMixin (object):
    def get_template_names (self):
        names = super(EventKindMixin, self).get_template_names()
        if hasattr(self, 'event'):
            event = self.event
        else:
            event = self.object
        suffix = '_' + event.kind.slug
        with_suffixes = []
        for name in names:
            pos = name.find('.')
            if pos < 0:
                with_suffixes.append(name + suffix)
            else:
                with_suffixes.append(name[:pos] + suffix + name[pos:])
            with_suffixes.append(name)
        return with_suffixes

class EventListView (EventMixin, ListView):
    pass
class EventListFutureView (EventMixin, ListView):
    def get_queryset (self):
        qs = super(EventListFutureView, self).get_queryset()
        return qs.filter(date_begin__gte=date.today())
class EventDetailView (EventKindMixin, EventMixin, DetailView):
    pass
class EventCreateView (EventMixin, CreateView):
    pass
class EventUpdateView (EventKindMixin, EventMixin, UpdateView):
    pass
class EventDeleteView (EventKindMixin, EventMixin, DeleteView):
    pass

class EventListYearArchiveView (EventMixin, YearArchiveView):
    date_field = 'date_begin'
    make_object_list = True
    allow_future = True
    allow_empty = True

class EventListDefaultArchiveView (RedirectView):
    permanent = False
    def get_redirect_url (self):
        year = datetime.datetime.now().year
        return reverse('seminar-event-list-year-archive', args=(year,))

def time_to_ordinal (time):
    return time.hour * 60 + time.minute

class EventTimeTableView (EventDetailView):
    template_name = 'seminar/event_timetable.html'

    # These are times in minutes from midnight:
    day_begin    =  9 * 60
    day_end      = 17 * 60
    break_begin  = 12 * 60 + 30
    break_end    = 14 * 60
    break_factor = 0.5

    def get_context_data (self, **kwargs):
        context = super(EventTimeTableView, self).get_context_data(**kwargs)
        event = self.object
        days = event.day_list()
        day_ratio = 100. / len(days)

        times = event.session_set.aggregate(Min('time_begin'), Max('time_end'))
        if times['time_begin__min'] is None:
            day_begin = self.day_begin
            day_end = self.day_end
        else:
            day_begin = min(time_to_ordinal(times['time_begin__min']),
                    self.day_begin)
            day_end = max(time_to_ordinal(times['time_end__max']),
                    self.day_end)

        break_adjustment = ( (self.break_end - self.break_begin)
                * (1 - self.break_factor) )
        scale_length = day_end - day_begin - break_adjustment

        def time_ratio (time):
            if isinstance(time, datetime.time):
                value = time_to_ordinal(time)
            else:
                value = int(time)
            if value > self.break_end:
                value -= break_adjustment
            elif value > self.break_begin:
                value = (self.break_begin +
                    (value - self.break_begin) * self.break_factor)
            return (value - day_begin) * 100. / scale_length

        day_list = []
        for number, date in days:
            item_list = []
            for session in Session.objects.filter(date__exact=date):
                begin_ratio = time_ratio(session.time_begin)
                end_ratio = time_ratio(session.time_end)
                item_list.append({
                    'session': session,
                    'begin_ratio': begin_ratio,
                    'length_ratio': end_ratio - begin_ratio,
                })
            day_list.append({
                'number': number,
                'date': date,
                'begin_ratio': number * day_ratio,
                'length_ratio': day_ratio,
                'item_list': item_list,
            })

        context.update({
            'day_list': day_list,
            'hour_list': [
                (datetime.time(hour//60, hour%60), time_ratio(hour))
                for hour in range((day_begin+59)//60*60, day_end, 30)
                if hour <= self.break_begin or hour >= self.break_end ]
        })
        return context

# Views for talks

class EventPartMixin (EventKindMixin):
    def dispatch (self, *args, **kwargs):
        self.event = get_object_or_404(Event, slug=kwargs['event_slug'])
        return super(EventPartMixin, self).dispatch(*args, **kwargs)

    def get_context_data (self, **kwargs):
        context = super(EventPartMixin, self).get_context_data(**kwargs)
        context['event'] = self.event
        return context

    def get_initial (self):
        initial = super(EventPartMixin, self).get_initial()
        initial['event'] = self.event.id
        return initial

class TalkMixin (EventPartMixin):
    model = Talk

    def get_queryset (self):
        return Talk.objects.filter(event=self.event)

class TalkFormMixin (TalkMixin):
    form_class = TalkForm

    def get_context_data (self, **kwargs):
        context = super(TalkFormMixin, self).get_context_data(**kwargs)
        sessionformset = sessionformset_factory(self.event)
        if self.request.POST:
            context['speaker_formset'] = SpeakerFormSet(self.request.POST,
                    instance=self.object)
            context['session_formset'] = sessionformset(self.request.POST,
                    instance=self.object)
            context['attachment_formset'] = AttachmentFormSet(self.request.POST,
                    self.request.FILES, instance=self.object)
        else:
            context['speaker_formset'] = SpeakerFormSet(instance=self.object)
            context['session_formset'] = sessionformset(instance=self.object)
            context['attachment_formset'] = AttachmentFormSet(instance=self.object)
        return context

    def form_valid (self, form):
        context = self.get_context_data()
        session_formset = context['session_formset']
        if not session_formset.is_valid():
            return self.render_to_response(self.get_context_data(form=form))
        speaker_formset = context['speaker_formset']
        if not speaker_formset.is_valid():
            return self.render_to_response(self.get_context_data(form=form))
        attachment_formset = context['attachment_formset']
        if not attachment_formset.is_valid():
            return self.render_to_response(self.get_context_data(form=form))
        self.object = form.save()
        session_formset.instance = self.object
        session_formset.save()
        speaker_formset.instance = self.object
        speaker_formset.save()
        attachment_formset.instance = self.object
        attachment_formset.save()
        # Save again so that the name_list field is set correctly
        self.object = form.save()
        return HttpResponseRedirect(self.get_success_url())

class TalkListView (TalkMixin, ListView):
    pass
class TalkDetailView (TalkMixin, DetailView):
    pass
class TalkCreateView (TalkFormMixin, CreateView):
    pass
class TalkUpdateView (TalkFormMixin, UpdateView):
    pass
class TalkDeleteView (TalkMixin, DeleteView):
    pass


# Views for sessions

class SessionMixin (EventPartMixin):
    model = Session

    def get_form_class (self):
        return sessionform_factory(self.event)

    def get_queryset (self):
        return Session.objects.filter(
                date__gte=self.event.date_begin,
                date__lte=self.event.date_end)

    def get_success_url (self):
        return reverse('seminar-session-list', args=(self.event.slug,))

class SessionListView (SessionMixin, ListView):
    pass
class SessionDetailView (SessionMixin, DetailView):
    pass
class SessionCreateView (SessionMixin, CreateView):
    pass
class SessionUpdateView (SessionMixin, UpdateView):
    pass
class SessionDeleteView (SessionMixin, DeleteView):
    pass


# ICal feed for sessions

class SessionFeed (ICalFeed):
    timezone = settings.TIME_ZONE

    def items (self):
        return Session.objects.all().order_by('-date', '-time_begin')

    def item_title (self, item):
        return item.name

    def item_description (self, item):
        if item.talk:
            return item.talk.abstract
        return None

    def item_start_datetime (self, item):
        return item.datetime_begin

    def item_end_datetime (self, item):
        return item.datetime_end

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.db.models.deletion
import seminar.models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Attachment',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=80, help_text='The name of the attachment')),
                ('file', models.FileField(upload_to=seminar.models.attachment_upload_path, blank=True, help_text='A file to upload')),
                ('url', models.URLField(blank=True, help_text='The URL of the attachment')),
            ],
            options={
                'ordering': ('name',),
            },
        ),
        migrations.CreateModel(
            name='Event',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', auto_created=True, primary_key=True)),
                ('title', models.CharField(blank=True, verbose_name='title', max_length=40, help_text='An optional title for the event')),
                ('slug', models.SlugField(help_text='A symbolic name for use in URLs (a default value is computed from the title and date)', verbose_name='slug', unique=True)),
                ('date_begin', models.DateField(verbose_name='first day', help_text='The date of the first day of the event')),
                ('date_end', models.DateField(verbose_name='last day', help_text='The date of the last day of the event')),
                ('short_description', models.TextField(blank=True, verbose_name='short description', help_text='A short presentation, in Markdown format')),
                ('long_description', models.TextField(blank=True, verbose_name='long description', help_text="Details to put on the event's page, in Markdown format")),
                ('post_scriptum', models.TextField(blank=True, verbose_name='post-scriptum', help_text='Comments to insert after the programme, in Markdown format')),
            ],
            options={
                'ordering': ['-date_begin'],
            },
        ),
        migrations.CreateModel(
            name='EventKind',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', auto_created=True, primary_key=True)),
                ('slug', models.SlugField(verbose_name='slug', help_text='A symbolic name, for use e.g. in CSS')),
                ('name', models.CharField(verbose_name='name', max_length=80, help_text='A verbose name for display')),
                ('template_event_base', models.CharField(verbose_name='base template', default='seminar/event_base.html', max_length=100, help_text='A base template for event pages of this kind')),
                ('template_session_base', models.CharField(verbose_name='base template', default='seminar/session_base.html', max_length=100, help_text='A base template for session pages of this kind')),
                ('template_talk_base', models.CharField(verbose_name='base template', default='seminar/talk_base.html', max_length=100, help_text='A base template for talk pages of this kind')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Session',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', auto_created=True, primary_key=True)),
                ('date', models.DateField(verbose_name='date', help_text='The day when this item occurs')),
                ('time_begin', models.TimeField(verbose_name='starting time', help_text='The scheduled time of beginning')),
                ('time_end', models.TimeField(verbose_name='ending time', help_text='The scheduled time of ending')),
                ('title', models.CharField(blank=True, verbose_name='title', null=True, max_length=120, help_text='A short description, or a subtitle for a talk')),
                ('event', models.ForeignKey(help_text='The event this session is part of', editable=False, on_delete=django.db.models.deletion.CASCADE, to='seminar.Event')),
            ],
            options={
                'ordering': ('date', 'time_begin'),
            },
        ),
        migrations.CreateModel(
            name='Speaker',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', auto_created=True, primary_key=True)),
                ('rank', models.IntegerField(help_text='The position in the list of speakers', editable=False)),
                ('name', models.CharField(blank=True, verbose_name='name', null=True, max_length=120)),
                ('web_page', models.URLField(blank=True, verbose_name='web page')),
                ('institution', models.CharField(blank=True, verbose_name='institution', null=True, max_length=120)),
            ],
            options={
                'ordering': ('talk', 'rank', 'name'),
            },
        ),
        migrations.CreateModel(
            name='Talk',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', auto_created=True, primary_key=True)),
                ('title', models.CharField(blank=True, verbose_name='title', null=True, max_length=200, help_text='A title or a short description')),
                ('slug', models.SlugField(verbose_name='slug', help_text="A symbolic name for use in URLs (a default value is computed from the speakers' names)")),
                ('abstract', models.TextField(blank=True, verbose_name='abstract', null=True, help_text='An abstract or long description, in Markdown format')),
                ('featured', models.BooleanField(verbose_name='featured', default=True, help_text='Whether this talk appears in the short announcement')),
                ('name_list', models.CharField(help_text='The list of speaker names, automatically computed', default='', max_length=120, editable=False)),
                ('event', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='seminar.Event')),
            ],
            options={
                'ordering': ('name_list', 'title'),
            },
        ),
        migrations.CreateModel(
            name='TalkKind',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', auto_created=True, primary_key=True)),
                ('slug', models.SlugField(verbose_name='slug', help_text='A symbolic name, for use e.g. in CSS')),
                ('name', models.CharField(verbose_name='name', max_length=80, help_text='A verbose name for display')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.AddField(
            model_name='talk',
            name='kind',
            field=models.ForeignKey(help_text='The kind of item in the schedule', on_delete=django.db.models.deletion.CASCADE, to='seminar.TalkKind', verbose_name='kind'),
        ),
        migrations.AddField(
            model_name='speaker',
            name='talk',
            field=models.ForeignKey(help_text='The talk this speaker is defined for', on_delete=django.db.models.deletion.CASCADE, related_name='speakers', to='seminar.Talk'),
        ),
        migrations.AddField(
            model_name='session',
            name='talk',
            field=models.ForeignKey(blank=True, help_text='The talk for this session, if any', null=True, on_delete=django.db.models.deletion.CASCADE, to='seminar.Talk', verbose_name='talk'),
        ),
        migrations.AddField(
            model_name='event',
            name='kind',
            field=models.ForeignKey(help_text='The kind of event', on_delete=django.db.models.deletion.CASCADE, to='seminar.EventKind', verbose_name='kind'),
        ),
        migrations.AddField(
            model_name='attachment',
            name='talk',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='seminar.Talk'),
        ),
        migrations.AlterUniqueTogether(
            name='talk',
            unique_together=set([('event', 'slug')]),
        ),
    ]

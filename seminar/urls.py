from django.contrib.auth.decorators import *
from django.urls import include, re_path

from seminar.views import *

event_patterns = [
    re_path(r'^$',
        EventDetailView.as_view(),
        name='seminar-event-detail'),
    re_path(r'^edit/$',
        permission_required('seminar.change_event')(EventUpdateView.as_view()),
        name='seminar-event-edit'),
    re_path(r'^delete/$',
        permission_required('seminar.delete_event')(EventDeleteView.as_view()),
        name='seminar-event-delete'),

    # talks

    re_path(r'^talks/$',
        TalkListView.as_view(),
        name='seminar-talk-list'),
    re_path(r'^talks/new_talk/$',
        permission_required('seminar.add_talk')(TalkCreateView.as_view()),
        name='seminar-talk-create'),
    re_path(r'^talks/(?P<slug>[-a-z0-9]+)/$',
        TalkDetailView.as_view(),
        name='seminar-talk-detail'),
    re_path(r'^talks/(?P<slug>[-a-z0-9]+)/edit/$',
        permission_required('seminar.change_talk')(TalkUpdateView.as_view()),
        name='seminar-talk-edit'),
    re_path(r'^talks/(?P<slug>[-a-z0-9]+)/delete/$',
        permission_required('seminar.delete_talk')(TalkDeleteView.as_view()),
        name='seminar-talk-delete'),

    # sessions

    re_path(r'^sessions/$',
        SessionListView.as_view(),
        name='seminar-session-list'),
    re_path(r'^sessions/new_session/$',
        permission_required('seminar.add_session')(SessionCreateView.as_view()),
        name='seminar-session-create'),
    re_path(r'^sessions/(?P<pk>\d+)/$',
        SessionDetailView.as_view(),
        name='seminar-session-detail'),
    re_path(r'^sessions/(?P<pk>\d+)/edit/$',
        permission_required('seminar.change_session')(SessionUpdateView.as_view()),
        name='seminar-session-edit'),
    re_path(r'^sessions/(?P<pk>\d+)/delete/$',
        permission_required('seminar.delete_session')(SessionDeleteView.as_view()),
        name='seminar-session-delete'),

    re_path(r'^timetable/$',
        EventTimeTableView.as_view(),
        name='seminar-event-timetable'),
]

urlpatterns = [
    re_path(r'^$',
        EventListView.as_view(),
        name='seminar-event-list'),
    re_path(r'^archives/$',
        EventListDefaultArchiveView.as_view(),
        name='seminar-event-list-archive'),
    re_path(r'^archives/(?P<year>\d{4})/$',
        EventListYearArchiveView.as_view(),
        name='seminar-event-list-year-archive'),
    re_path(r'^new_event/$',
        permission_required('seminar.add_event')(EventCreateView.as_view()),
        name='seminar-event-create'),
    re_path(r'^sessions.ics$',
        SessionFeed(),
        name='seminar-session-feed'),
    re_path(r'^(?P<event_slug>[-a-z0-9]+)/', include(event_patterns)),
]

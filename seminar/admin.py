from django.contrib import admin, auth
from django.contrib.auth.models import User

from seminar.models import *

class AttachmentInline (admin.TabularInline):
    model = Attachment
class SpeakerInline (admin.TabularInline):
    model = Speaker
class SessionInline (admin.StackedInline):
    model = Session
class TalkAdmin (admin.ModelAdmin):
    inlines = [SpeakerInline, SessionInline, AttachmentInline]

class TalkInline (admin.StackedInline):
    model = Talk
    inlines = [SpeakerInline, SessionInline, AttachmentInline]
class EventAdmin (admin.ModelAdmin):
    inlines = [TalkInline, SessionInline]

admin.site.register(EventKind)
admin.site.register(Event, EventAdmin)
admin.site.register(TalkKind)
admin.site.register(Talk, TalkAdmin)
admin.site.register(Session)

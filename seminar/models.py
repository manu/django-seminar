import datetime
import string
import sys

from django import VERSION
from django.contrib.auth.models import *
from django.core.exceptions import ValidationError
from django.db.models import *
from django.template.defaultfilters import slugify
from django.utils.html import escape
from django.utils.safestring import mark_safe
from django.utils.translation import gettext_lazy as _

if VERSION >= (2, 0):
    from django.urls import reverse
else:
    from django.core.urlresolvers import reverse

class UnicodeMixin(object):
    """
    Mixin class to handle defining the proper __str__/__unicode__
    methods in Python 2 or 3 (taken from Python's documentation).
    """
    if sys.version_info[0] >= 3:  # Python 3
        def __str__(self):
            return self.__unicode__()
    else:  # Python 2
        def __str__(self):
            return self.__unicode__().encode('utf8')

class SlugNKManager (Manager):
    """
    This is a manager that uses a model's slug field as a natural key.
    """
    def get_by_natural_key (self, slug):
        return self.get(slug=slug)

class SlugNKModel (Model):
    """
    This abstract class extends the base Model class using its slug field as a
    natural key.
    """
    class Meta:
        abstract = True

    objects = SlugNKManager()

    def natural_key (self):
        return (self.slug,)

class EventKind (UnicodeMixin, SlugNKModel):
    """
    Represents a kind of event, for presentation purposes.
    """
    slug = SlugField(verbose_name = _("slug"),
        help_text = _("A symbolic name, for use e.g. in CSS"))
    name = CharField(verbose_name = _("name"), max_length = 80,
        help_text = _("A verbose name for display"))
    template_event_base = CharField(verbose_name = _("base template"),
        max_length=100, default = 'seminar/event_base.html',
        help_text = _("A base template for event pages of this kind"))
    template_session_base = CharField(verbose_name = _("base template"),
        max_length=100, default = 'seminar/session_base.html',
        help_text = _("A base template for session pages of this kind"))
    template_talk_base = CharField(verbose_name = _("base template"),
        max_length=100, default = 'seminar/talk_base.html',
        help_text = _("A base template for talk pages of this kind"))

    def __unicode__ (self):
        return self.name
    objects = SlugNKManager()

class Event (UnicodeMixin, SlugNKModel):
    """
    Represents an event.
    """
    kind = ForeignKey(EventKind, PROTECT, verbose_name = _("kind"),
        help_text = _("The kind of event"))
    title = CharField(verbose_name = _("title"), max_length = 80, blank = True,
        help_text = _("An optional title for the event"))
    slug = SlugField(verbose_name = _("slug"), unique = True,
        help_text = _("A symbolic name for use in URLs"
            " (a default value is computed from the title and date)"))
    date_begin = DateField(verbose_name = _("first day"),
        help_text = _("The date of the first day of the event"))
    date_end = DateField(verbose_name = _("last day"),
        help_text = _("The date of the last day of the event"));
    short_description = TextField(verbose_name = _("short description"), blank = True,
        help_text = _("A short presentation, in Markdown format"))
    long_description = TextField(verbose_name = _("long description"), blank = True,
        help_text = _("Details to put on the event's page, in Markdown format"))
    post_scriptum = TextField(verbose_name = _("post-scriptum"), blank = True,
        help_text = _("Comments to insert after the programme, in Markdown format"))

    class Meta:
        ordering = ['-date_begin']

    @property
    def name (self):
        if self.title:
            return self.title
        return self.kind.name

    def __unicode__ (self):
        return '%s, %s' % (self.name, self.date_begin)

    def get_absolute_url (self):
        return reverse('seminar-event-detail', args=[str(self.slug)])

    def save (self, *args, **kwargs):
        if not self.slug:
            self.slug = find_unused_slug(
                self.kind.slug + '-' + self.date_begin.strftime('%Y-%m-%d'),
                Event.objects.all())
        super(Event, self).save(*args, **kwargs)

    def day_list (self):
        """
        Return the list of pairs (day number, date) for each day contained in
        the week. The day number starts at 0.
        """
        first = self.date_begin.toordinal()
        last = self.date_end.toordinal()
        return [
            (day - first, datetime.date.fromordinal(day))
            for day in range(first, last+1) ]

    def talks_by_kind (self):
        return self.talk_set.order_by(*(('kind',) + Talk._meta.ordering))

    def featured_talks (self):
        return self.talk_set.filter(featured=True)

    def schedule_is_set (self):
        talks = self.talk_set.annotate(num_sessions=Count('session'))
        return not talks.filter(num_sessions=0).exists()

def find_unused_slug (base, queryset):
    """
    Given a base slug, return a slug that is unused in the given queryset's
    objects' 'slug' fields, by appending a dash and a number if necessary.
    """
    slugs = [obj.slug for obj in queryset.filter(slug__startswith=base)]
    slug = base
    count = 0
    while slug in slugs:
        count += 1
        slug = '%s-%d' % (base, count)
    return slug

class TalkKind (UnicodeMixin, SlugNKModel):
    """
    Represents a kind of talk, for presentation purposes.
    """
    slug = SlugField(verbose_name = _("slug"),
        help_text = _("A symbolic name, for use e.g. in CSS"))
    name = CharField(verbose_name = _("name"), max_length = 80,
        help_text = _("A verbose name for display"))

    def __unicode__ (self):
        return self.name

class TalkManager (Manager):
    """
    This manager use a pair of an event slug and a talk slug as natural key.
    """
    def get_by_natural_key (self, event_slug, talk_slug):
        return self.get(event__slug=event_slug, slug=talk_slug)

class Talk (UnicodeMixin, Model):
    """
    Represents a talk, inside an event.
    """
    event = ForeignKey(Event, CASCADE, editable = True)
    kind = ForeignKey(TalkKind, PROTECT, verbose_name = _("kind"),
            help_text = _("The kind of item in the schedule"))
    title = CharField(verbose_name = _("title"),
            max_length = 200, blank = True, null = True,
            help_text = _("A title or a short description"))
    slug = SlugField(verbose_name = _("slug"),
            help_text = _("A symbolic name for use in URLs"
                " (a default value is computed from the speakers' names)"))
    abstract = TextField(verbose_name = _("abstract"), null = True, blank = True,
            help_text = _("An abstract or long description, in Markdown format"))
    featured = BooleanField(verbose_name = _("featured"), default = True,
            help_text = _("Whether this talk appears in the short announcement"))

    name_list = CharField(max_length = 120, default = "", editable = False,
            help_text = _("The list of speaker names, automatically computed"))

    class Meta:
        ordering = ('name_list', 'title')
        unique_together = (('event', 'slug'),)

    objects = TalkManager()

    def natural_key (self):
        return (self.event.slug, self.slug)

    @property
    def name (self):
        if self.speakers is not None:
            text = ", ".join([s.display_name for s in self.speakers.all()])
        else:
            text = ""
        if self.title:
            if len(text) != 0:
                text += " - "
            text += self.title
        if len(text) == 0:
            text = self.kind.name
        return text

    def __unicode__ (self):
        return self.name

    def save (self, *args, **kwargs):
        # The slug field is computed when not provided and the name_list field
        # is always computed. Both are deduced from the list of speakers,
        # which is stored as a relation (created by the Speaker class).
        # Accessing this list requires that the Talk object has a primary key,
        # but this key is provided by the database, so we must call save()
        # before computing these fields when there is no primary key yet, i.e.
        # when creating a new object.
        if not self.pk:
            super(Talk, self).save(*args, **kwargs)
        if not self.slug:
            self.slug = find_unused_slug(
                "-".join([ slugify(s.name.split(', ')[0])
                    for s in self.speakers.all() ]),
                Talk.objects.filter(event=self.event))
        self.name_list = "; ".join([s.name for s in self.speakers.all()])
        super(Talk, self).save(*args, **kwargs)

    def get_absolute_url (self):
        return reverse('seminar-talk-detail',
                args=[self.event.slug, self.slug])

class Speaker (Model):
    """
    Represents a speaker for a talk, with a name, a web page, etc.
    """
    talk = ForeignKey(Talk, CASCADE, related_name='speakers',
            help_text = _("The talk this speaker is defined for"))
    rank = IntegerField(editable = False,
            help_text = _("The position in the list of speakers"))
    name = CharField(verbose_name = _("name"),
            max_length = 120, blank = True, null = True)
    web_page = URLField(verbose_name = _("web page"),
            blank = True)
    institution = CharField(verbose_name = _("institution"),
            max_length = 120, blank = True, null = True)

    class Meta:
        ordering = ('talk', 'rank', 'name')

    @property
    def display_name (self):
        if not self.name:
            return ""
        parts = self.name.split(", ")
        if len(parts) == 2:
            parts.reverse()
        elif len(parts) >= 3:
            parts = [parts[2], parts[0], parts[1]]
        return " ".join(parts)

    @property
    def short_name (self):
        if not self.name:
            return ""
        return self.name.split(", ")[0]

class Session (UnicodeMixin, Model):
    """
    Represents a session in a schedule, possibly associated to a talk.
    """
    event = ForeignKey(Event, CASCADE, editable = False,
            help_text = _("The event this session is part of"))
    talk = ForeignKey(Talk, CASCADE, verbose_name = _("talk"), blank = True, null = True,
            help_text = _("The talk for this session, if any"))
    date = DateField(verbose_name = _("date"),
            help_text = _("The day when this item occurs"))
    time_begin = TimeField(verbose_name = _("starting time"),
            help_text = _("The scheduled time of beginning"))
    time_end = TimeField(verbose_name = _("ending time"),
            help_text = _("The scheduled time of ending"))
    title = CharField(verbose_name = _("title"),
            max_length = 120, blank = True, null = True,
            help_text = _("A short description, or a subtitle for a talk"))

    class Meta:
        ordering = ('date', 'time_begin')

    @property
    def name (self):
        if self.title:
            if self.talk:
                return "%s - %s" % (self.talk.name, self.title)
            return self.title
        if self.talk:
            return self.talk.name
        if self.event:
            return self.event.name
        return _("Session")

    @property
    def datetime_begin (self):
        return datetime.datetime.combine(self.date, self.time_begin)

    @property
    def datetime_end (self):
        return datetime.datetime.combine(self.date, self.time_end)

    def __unicode__ (self):
        return "%s, %s, %s" % (self.name, self.date, self.times())

    def times (self):
        """
        Returns a string that contains the start and end times of the session.
        """
        return '%d:%02d - %d:%02d' % (
            self.time_begin.hour, self.time_begin.minute,
            self.time_end.hour, self.time_end.minute)

    def get_absolute_url (self):
        return reverse('seminar-session-detail',
                args=[self.event.slug, self.id])

def attachment_upload_path (instance, filename):
    return 'talk%d/%s' % (instance.talk.id, filename)

class Attachment (Model):
    """
    A file or an URL attached to a talk.
    """
    talk = ForeignKey(Talk, CASCADE, editable = True)
    name = CharField(max_length = 80,
            help_text = _("The name of the attachment"))
    file = FileField(upload_to = attachment_upload_path, blank = True,
            help_text = _("A file to upload"))
    url = URLField(blank = True,
            help_text = _("The URL of the attachment"))

    class Meta:
        ordering = ('name',)

    def clean (self):
        return
        if (self.file and self.url) or not (self.file or self.url):
            raise ValidationError("Either a file or an URL is required.")

    def get_url (self):
        if self.file:
            return self.file.url
        else:
            return self.url

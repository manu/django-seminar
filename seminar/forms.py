from django.forms import ModelForm, ModelChoiceField
from django.forms.models import BaseInlineFormSet, inlineformset_factory
from django.forms.widgets import *

from seminar.models import *

class EventForm (ModelForm):
    class Meta:
        model = Event
        fields = (
                'kind',
                'title',
                'date_begin',
                'date_end',
                'short_description',
                'long_description',
                'post_scriptum',
        )
        widgets = {
            'short_description': Textarea(attrs={'cols': 80, 'rows': 5}),
            'long_description': Textarea(attrs={'cols': 80, 'rows': 5}),
            'post_scriptum': Textarea(attrs={'cols': 80, 'rows': 5}),
        }

def sessionform_factory (event):
    day_list = event.day_list()
    if len(day_list) > 1:
        choices = [('', '----')] + [
            (date, date.strftime('%Y-%m-%d (%A)')) for (_, date) in day_list]
    else:
        choices = None
        day = day_list[0][1]
    class SessionForm (ModelForm):
        talk = ModelChoiceField(queryset=Talk.objects.filter(event=event),
                required=False)
        class Meta:
            model = Session
            fields = (
                    'talk',
                    'date',
                    'time_begin',
                    'time_end',
                    'title',
            )
            if choices is None:
                exclude = ( 'date', )
            else:
                widgets = { 'date': Select(choices=choices) }
        def save (self, commit=True):
            instance = super(SessionForm, self).save(commit=False)
            instance.event = event
            if choices is None:
                instance.date = day
            if commit:
                instance.save()
            return instance
    return SessionForm

def sessionformset_factory (event):
    sessionform = sessionform_factory(event)
    return inlineformset_factory(Talk, Session, form=sessionform, extra=1)

class SpeakerForm (ModelForm):
    class Meta:
        model = Speaker
        fields = (
            'talk',
            'name',
            'web_page',
            'institution',
        )

    def clean_name (self):
        return normalize_name(self.cleaned_data['name'])

class BaseOrderedInlineFormSet (BaseInlineFormSet):
    def save_existing_objects (self, commit=True):
        changed = super(BaseOrderedInlineFormSet, self).save_existing_objects(commit=commit)
        not_deleted = [form.save(commit=False)
                for form in self.initial_forms
                if not self._should_delete_form(form)]
        return not_deleted

    def save (self, commit=True):
        instances = super(BaseOrderedInlineFormSet, self).save(commit=False)
        rank = 1
        for instance in instances:
            instance.rank = rank
            if commit:
                instance.save()
            rank += 1
        if commit:
            self.save_m2m()
        return instances

SpeakerFormSet = inlineformset_factory(Talk, Speaker,
        formset=BaseOrderedInlineFormSet, form=SpeakerForm, extra=1)

AttachmentFormSet = inlineformset_factory(Talk, Attachment, extra=1,
        fields=('talk','name','file','url',))

class TalkForm (ModelForm):
    class Meta:
        model = Talk
        fields = (
            'event',
            'kind',
            'title',
            'abstract',
            'featured',
        )
        widgets = {
            'event': HiddenInput(),
            'abstract': Textarea(attrs={'cols': 80, 'rows': 10}),
        }

def normalize_name (text):
    "Normalize the speaker's name according to BibTeX parsing rules."
    parts = text.strip().split(',')
    if len(parts) == 0:
        return ""

    if len(parts) == 1:
        words = parts[0].split()
        step = 'first'
        von_start = 0
        while von_start < len(words) - 1 and upper_or_caseless(words[von_start]):
            von_start += 1
        first = words[:von_start]
        von_last = words[von_start:]
        jr = []

    elif len(parts) == 2:
        first = parts[1].split()
        von_last = parts[0].split()
        jr = []

    else:
        von_last = parts[0].split()
        jr = parts[1].split()
        for part in parts[2:]:
            words = part.split()
            if len(words) == 0:
                continue
            words[-1] += ","
            first.extend(words)

    text = " ".join(von_last)
    if len(jr) != 0:
        text += ", " + " ".join(jr)
    if len(first) != 0:
        text += ", " + " ".join(first)
    return text

def upper_or_caseless (text):
    for c in text:
        if c.isupper():
            return True
        if c.isdigit() or c.islower():
            return False
    return True

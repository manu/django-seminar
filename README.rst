================
 Django-Seminar
================

Django-Seminar is a Django application designed to handle schedules for events
like conferences, workshops or seminars. Each event contains a list of talks
(each with a title, abstract and list of speakers) and a schedule. Views are
provided to display and edit each element, with a default URL scheme and
default templates.


Requirements
============

This version of Django-Seminar requires Django version 1.9 or above.

The default templates use Markdown to format blocks of text, which relies on
`django-markdown-deux`_, or any django app providing a ``markdown`` template
tag.

.. _django-markdown-deux: https://github.com/trentm/django-markdown-deux/

The ICal feed is generated using ``django-ical``.


Installation
============

You can install the application using the standard Python procedure:

    python setup.py install

Integration in a Django project is standard:

1. Add ``seminar`` to the ``INSTALLED_APPS`` setting.

2. Include the URLconf in the project's URL dispatcher, for instance:

   url(r'^events/', include('seminar.urls'))

3. Create the models in the database:

   python manage.py migrate

4. Use the admin interface to create at least one ``EventKind`` object and one
   ``TalkKind`` object, otherwise you will not be able to create any events or
   talks (see below for details on the structure of data).

A minimal example project using Django-Seminar is provided in the ``example``
directory and can be used with the provided ``manage.py`` script. It follows
the default project structure as provided by Django 4.2 but works with Django
versions back to 2.0. Assuming the dependencies are installed, you can run it
directly by saying

    python manage.py migrate
    python manage.py createsuperuser
    python manage.py runserver


Usage
=====

Models
------

The application is structured around three main models:

- ``Event`` is the top-level object, it represents a single event, for which
  the application handles a programme.

- ``Talk`` represents a logical part of the programme of an event, like a
  course or an invited talk. It has a title and an absract but is not located
  in time. It may, however, correspond to one or several sessions in the
  event's schedule.

- ``Session`` represents a slot in an event's schedule. It may be associated
  to a ``Talk`` if it represents the slot for a talk or a part of it. It may
  also be independent, in order to represent non-talk parts of a schedule,
  like breaks, lunches or any other sessions.

- ``EventKind`` represents a kind of event, ``TalkKind`` represents a kind of
  talk. Different kinds can be used to obtain different presentations, using
  different templates. The ``Event`` and ``Talk`` models have a ``kind`` field
  pointing to a kind object of the appropriate type.

Views
-----

A default URL scheme is provided, using the ``slug`` fields of the ``Event``
and ``Talk`` models. The scheme works as follows:

- ``<event_slug>/`` for the main page of an event
- ``<event_slug>/talks/`` for the list of talks
- ``<event_slug>/talks/<talk_slug>/`` for details on a given talk
- ``<event_slug>/sessions/`` for the schedule
- ``<event_slug>/sessions/<session_id>/`` for details on a session
- ``<event_slug>/timetable/`` for a calendar-like timetable
- ``archives/<year>/`` provides a list of events for a given yer
- ``archives`` redirects to the archives for the current year
- ``sessions.ics`` provides an ICal feed of all sessions
- the root URL provides a list of events

Additional URLs are used for the create/update/delete cycle, of course.

URLs are named using the scheme ``seminar-<model>-<action>``, where
``<action>`` is ``list``, ``detail``, ``create``, ``edit`` or ``delete``.
All URLs expect an event slug as argument, except for ``seminar-event-list``
and ``seminar-event-create``. Talk and session views related to a given
instance expect the slug (for talks) or id (for sessions) as a second
argument.

Views are implemented by classes derived from Django's generic views. Each
view has a class named ``<Model><Action>View``, where ``<Action>`` is
``List``, ``Detail``, ``Create``, ``Update`` or ``Delete``. Timetables are
produced by the class named ``EventTimeTableView``. All views except
``EventListView`` and ``EventCreateView`` expect a named argument
``event_slug`` that identifies the event.

Template scheme
---------------

Default templates are provided, using the standard naming scheme of
``seminar/<model>_<action>.html`` where ``<action>`` is ``list``, ``detail``,
``form`` or ``confirm_delete``. There is also a template called
``seminar/event_timetable.html`` for timetable views.

For each view that depends on a particular event object, that is all views
except event list and event creation, a template named
``seminar/<model>_<action>_<kind_slug>.html`` is tried before the standard
template. The ``<kind_slug>`` is the value of the ``slug`` field in the
``EventKind`` object for the event. Using this, different templates can be
used for different event kinds.

Base templates are used for common parts of the templates:

- ``seminar/base.html`` extends ``base.html`` (which is not provided),
  it is extended by ``event_form.html`` and ``event_list.html`` and by other
  base templates;

Other base templates are provided for events, talks and sessions. Their names
are stored in the fields named ``template_<model>_base`` in the ``EventKind``
model. The default values correspond to provided templates:

- ``seminar/event_base.html`` extends ``seminar/base.html``, it is extended by
  event-specific templates, it expects a context variable ``event``;

- ``seminar/talk_base.html`` extends ``seminar/event_base.html`` and is
  extended by detail and delete templates for talks, assuming an extra context
  variable ``talk``;

- ``seminar/session_base.html`` extends ``seminar/event_base.html`` and is
  extended by detail and delete templates for sessions, assuming an extra
  context variable ``session``.

Additional templates are provided for parts of the contents, they may be
included in other templates with appropriate context variables.

- ``seminar/event_div.html`` produces a DIV block with a short description of
  an event, it is used by ``seminar/event_list.html``;
- ``seminar/session_list_content.html`` produces the actual schedule of an
  event, so that it may be included in other templates than the session list;
- ``seminar/talk_list_content.html`` produces the actual list of talks of an
  event, so that it may be included in other templates than the talk list;
- ``seminar/talk_list_short.html`` produces the actual list of talks of an
  event in short format, it is used in particular in ``event_div.html``;
- ``seminar/talk_item.html`` produces the text describing a talk, to be
  included inside an LI block in talk lists.

A template filter is defined in a tag library ``seminar`` for presentation
purposes:

- ``speaker|name_reference`` produces the name of the speaker, with a link to
  the web page if it is defined, and the institution name in parentheses.

The following template blocks are used:

- ``title`` for the page title, is used by the base templates;
- ``navigation_links`` for navigation items is also used by the base
  templates, it is filled with ``li`` elements containing ``a`` links;
- ``admin_links`` for navigation items is also used by the base
  templates, it is filled with ``li`` elements containing ``a`` links for
  create/edit/delete views;
- ``content``, for the actual page contents, is used by the main templates;
- ``extra_head``, for material to put in the HTML header, is used in form
  templates for form media.

The provided templates use a lot of classes for HTML elements. All class names
starts with ``seminar_``. A default CSS style file is provided as
``static/css/seminar.css``.

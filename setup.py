from distutils.command.build import build
from distutils.core import setup

import os

root_path = os.path.normpath(os.path.join(os.path.abspath(__file__), os.pardir))
os.chdir(root_path)

class build_with_mo (build):
    """Build package files, including message catalogs for Django."""
    def run (self):
        from django.core.management import call_command
        os.chdir('seminar')
        call_command('compilemessages')
        os.chdir(root_path)
        build.run(self)

setup(
    name='django-seminar',
    version='1.0',
    author='Emmanuel Beffara',
    author_email='manu@beffara.org',
    packages=[
        'seminar',
        'seminar.migrations',
        'seminar.templatetags',
    ],
    package_data={
        'seminar': [
            'locale/*/*/*.mo',
            'locale/*/*/*.po',
            'static/css/*.css',
            'templates/seminar/*.html',
        ]},
    license='BSD',
    description='A Django application to handle schedules for conferences or seminars.',
    url='https://git.framasoft.org/manu/django-seminar',
    classifiers=[
        'Environment :: Web Environment',
        'Framework :: Django',
        'Framework :: Django :: 2.0',
        'Framework :: Django :: 2.1',
        'Framework :: Django :: 2.2',
        'Framework :: Django :: 3.0',
        'Framework :: Django :: 3.1',
        'Framework :: Django :: 3.2',
        'Framework :: Django :: 4.0',
        'Framework :: Django :: 4.1',
        'Framework :: Django :: 4.2',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3',
        'Topic :: Internet :: WWW/HTTP',
        'Topic :: Internet :: WWW/HTTP :: Dynamic Content',
    ],
    cmdclass={'build': build_with_mo},
)
